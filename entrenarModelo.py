# USAGE
# script para entrenar el modelo

# importanto librerias necesarias para su ejecución
# procesador de imagenes
from tensorflow.keras.preprocessing.image import ImageDataGenerator
# extracción de caracteristicas
from tensorflow.keras.applications import MobileNetV2
# agrupacion de datos espaciales
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os


dataset = "dataset/data/"
plot="dataset"

# parametros dpara entrenar el modelo
# tasa de aprendisaje, número de epocas, tamaño del lote
INIT_LR = 1e-4
EPOCHS = 20
BS = 32

print("[INFO] loading images...")
# obtener lista de las direcciones de las imagenes que se van usar 
# para el modelo
imagePaths = list(paths.list_images(dataset))
print(imagePaths)
dato = []
labels = []

# recorrer lista de imagenes
for imagePath in imagePaths:
	print(imagePath)
	# extraer nombre de la carpeta de las imagenes que vamos usar
	label = imagePath.split(os.path.sep)[-2]
	# cargar imagen de entrar reduciendo la resolucion
	image = load_img(imagePath, target_size=(224, 224))
	# convertir la imagen en una matriz
	image = img_to_array(image)
	# preprocesar matriz donde se encuentra codificada la imagene
	image = preprocess_input(image)
	# actualizar lista de datos y etiquetas
	dato.append(image)
	labels.append(label)

# converitr los datos de las imagenes y sus etiquetas correspondientes
# en una matriz
dato = np.array(dato, dtype="float32")
labels = np.array(labels)

# binalizar imagenes en una forma de uno contra otro
# consiste en aprender un clasififcador binario por clase
# en que las etiquetas binarias pertenesca a una sola clase 
lb = LabelBinarizer()
# los objetivos binarios se transforman en un vector de columna
labels = lb.fit_transform(labels)
# convierte un vector de clase en una matriz de clase binaria
labels = to_categorical(labels)

# particionar los datos en datos de prueba y entrenamiento 
# 80% entrenar - 20% pruebas
(trainX, testX, trainY, testY) = train_test_split(dato, labels,
	test_size=0.20, stratify=labels, random_state=42)

# construir generador de imagenes de entrenamiento
aug = ImageDataGenerator(rotation_range=20,zoom_range=0.15,width_shift_range=0.2,height_shift_range=0.2,
	shear_range=0.15,
	horizontal_flip=True,
	fill_mode="nearest")

# cargar red MobileNetV2 con un entrenamiento previo de imaginet
baseModel = MobileNetV2(weights="imagenet", include_top=False, input_tensor=Input(shape=(224, 224, 3)))

# construcción de la cabezera del modelo que se coloca encima del modelo
headModel = baseModel.output
headModel = AveragePooling2D(pool_size=(7, 7))(headModel)
headModel = Flatten(name="flatten")(headModel)
headModel = Dense(128, activation="relu")(headModel)
headModel = Dropout(0.5)(headModel)
headModel = Dense(2, activation="softmax")(headModel)

# colocando cabecera modelo encima del modelo base
model = Model(inputs=baseModel.input, outputs=headModel)

# recorrer las capas del modelo y evitar que se actualicen durante el primer proceso 
# de entrenamiento
for layer in baseModel.layers:
	layer.trainable = False

# compilar modelo
print("[INFO] compiling model...")
opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model.compile(loss="binary_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# entrenando modelo
print("[INFO] training head...")
H = model.fit(
	aug.flow(trainX, trainY, batch_size=BS),
	steps_per_epoch=len(trainX) // BS,
	validation_data=(testX, testY),
	validation_steps=len(testX) // BS,
	epochs=EPOCHS)
# realizar predicciones con el conjunto de pruebas
print("[INFO] evaluating network...")
predIdxs = model.predict(testX, batch_size=BS)

# encontrar el indice de la etiqueta con la mayor probabilidad
predIdxs = np.argmax(predIdxs, axis=1)

# mostrar el informe de claificación
print(classification_report(testY.argmax(axis=1), predIdxs,target_names=lb.classes_))

# serializar modelo en disco
print("[INFO] saving mask detector model...")
model.save("mi_modelo.h5")
print(H)

# dibujar la perdida de entrenamiento y precision
N = EPOCHS
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(plot)
