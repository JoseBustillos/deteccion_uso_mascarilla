# importando pauetes necesaroi
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from imutils.video import VideoStream
import imutils
import numpy as np
import time
import cv2

# ruta al directorio del modelo del detector facial
face = 'res10_300x300_ssd_iter_140000.caffemodel'
modelCoffe = 'deploy.prototxt'
# ruta del modelo entrenado
model = 'mi_modelo.h5'
# confianza minima
confidenceMin = 0.8


def detect_and_predict_mask(frame, faceNet, maskNet):
    (h, w) = frame.shape[:2]
    # OPENcv proporciona funciones para facilitar el preprocesamientode imagenes para
    # clasificación de aprendizaje prifundo
    blob = cv2.dnn.blobFromImage(frame, 1.0, (300, 300), (104.0, 177.0, 123.0))
    print(blob)
    # pasar el blob a través de la red
    faceNet.setInput(blob)
    # obtener las detecciones de rostros
    detections = faceNet.forward()
    # inicializar nuestra lista de caras, sus ubicaciones correspondientes
    # y la lista de predicciones de nuestra red de mascarillas
    faces = []
    locs = []
    preds = []
    # bucle sobre las detecciones
    for i in range(0, detections.shape[2]):
        # extraer la probabilidad asociada con la detección
        confidence = detections[0, 0, i, 2]
        # asegurar que la confianza sea mayor que la confianza minima
        if confidence > confidenceMin:
            # calcular coordenadas X Y del cuadro delimitador de objetos
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            # asegurar que el cuadro delimitador se encuentra
            # dentor de las dimensiones del marco
            (startX, startY) = (max(0, startX), max(0, startY))
            (endX, endY) = (min(w - 1, endX), min(h - 1, endY))
            # extrayendo la imagen del rostro y convirtiendo de colores
            # a blanco y negro cambiando las dimensiones de 224, 224
            # y preprocesar imagen
            face = frame[startY:endY, startX:endX]
            face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
            face = cv2.resize(face, (224, 224))
            face = img_to_array(face)
            face = preprocess_input(face)
            face = np.expand_dims(face, axis=0)
            # agregando el rostro y los cuadros delimiradotres a sus listas
            faces.append(face)
            locs.append((startX, startY, endX, endY))
    # realizar predicciones si detecta un rostro
    if len(faces) > 0:
        # realizamos predicciones por lotes
        preds = maskNet.predict(faces)
    # retorna tupla de los rostros y sus ubicaciones
    return (locs, preds)


# cargar modelo de detector facial desde disco
print("[INFO] Cargando modelos entrenados por caffemodel...")
faceNet = cv2.dnn.readNet(modelCoffe, face)
# cargamos modelos de detector de mascarillas
print("[INFO] Cargando modelo de la red CNN propia de mascarillas...")
maskNet = load_model(model)
# empezar la transmision de video y permitir acceso al sensor de la camara
print("[INFO] Accediendo a la camara web del computador e inicializar video...")
vs = VideoStream(src=0).start()
print(vs)
time.sleep(2.0)
frame = vs.read()
frame = imutils.resize(frame, width=400)
while(True):
    # detectar rostros en el marco y determinar si están usando un mascarilla facial o no
    (locs, preds) = detect_and_predict_mask(frame, faceNet, maskNet)
    # recorrer los rostros y su ubicacion
    for (box, pred) in zip(locs, preds):
        # desempaquetar el cuadro delimitador y las predicciones
        (startX, startY, endX, endY) = box
        (mask, withoutMask) = pred
        # determinar etiqueta de la clase, el cuadro delimitador y texto
        print('texto')
        label = "Mascarilla" if mask > withoutMask else "NO lleva Mascarilla"
        color = (0, 255, 0) if label == "SI Lleva Mascarilla" else (0, 0, 255)
        print(label)
        # incluir probabilidad en la etiqueta
        label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)
        # mostrar la etiqueta y el retangulo
        cv2.putText(frame, label, (startX, startY - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
        cv2.rectangle(frame, (startX, startY), (endX, endY), color, 2)
        print('rectangulo')

    cv2.imshow("Frame", frame)
    key = cv2.waitKey(0)

    if key == ord("q"):
        break

        # mostrar marco de salirda
cv2.destroyAllWindows()
vs.stop()
